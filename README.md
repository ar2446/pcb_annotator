# Organizational

- Hard project deadline to finish the Webinterface: 9. March
- Final Presentation: 
	- 20. March 9:00 - 13:00
	- Room 301 (Albert-Einstein-Straße 22)


# Minimal Requirements (ALL of these MUST be fullfilled)

- When selecting a pad all its information is displayed in the "Pad 
Options" menu
- Changing the information of a selected pad in the "Pad Options" updates 
the pads information
- Exporting files must work correctly
- Loading files must work correctly
- Pad IDs must work
	- no empty ID is allowed
	- no ID may be used more than once 
- The pads must be better recognizable
	- fill the entire pads with color not only a dashed line
	- stronger color (PINK instead of yellow)
- The currently selected pad must be easily recognizable
	different color than other pads (CYAN)
- Remove all interface items that do not work (Zoom, Component Options, 
...)


# PCB_Annotator

![paper_prototype](./Paper%20Prototype/Prototyp.png "Paper-Prototype")

## Requirements



The pads of an AutoTracer are connected to the individual solder-pads of the PCB that will be analysed. To conduct a meaningfull analysis a mapping between AutoTracer-IO-Pads and the positions on the PCB is neccessary. 

For each solderpad on the PCB some attributes can be assigned (Name, connected Scanner-Port and Component-ID). The individual solderpoints on solderpads on the PCB can be visually marked using rectangles that are placed on top of the image using the mouse. The rectangles attributes (x-position, y-position, height and width) are stored together with the pad-attributes.

Multiple components can be grouped together into a component. For each component a Name and a type can be defined.

The resulting data can be exported as a JSON-file and also be imported at a later point in time.

It is possible to switch between two images of the PCB (one of the front and one of the back). Also the rectangles that symbolise the solderpads can be shown and hidden.





# Found Bugs (Message from 8. February)

- When moving the cursor while creating a new pad the contents of the 
website is selected: 
	- Disable selection of the content using css
- The bottom tool-bar does not work at all. It should be possible to 
selected a tool
	- The tool should work
	- After executing one action with the tool the tool should stay 
selected
	- the currently active tool should be highlighted
- When creating a new item it can only be dragged from top left to bottom 
right
	- If it causes big effort to fix this it then leave it
- Selecting an existing component is impossible
- Only pads seem to work, components not at all
- In the exported file only the most recent pad is included
- In the exported file only the width, height, XPosition and YPosition are 
valid. All other parameters stay empty
- The image is destroying the layout of the interface by pushing the 
bottom menu down. The Image should be restricted to a valid size
- Image gets distorted when resizing the browser window and not everything 
of the image is visible
